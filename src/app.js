require('dotenv').config()
const express = require('express')
const morgan = require('morgan')

const serviceDoc = require('./docs/serviceDoc')

const app = express()

if(process.env.NODE_ENV !== 'test') {
  app.use(morgan('dev'))
}

app.use(require('./middleware/cors'))

app.get('/', (_req, res) => {
  res.json(serviceDoc)
})

app.use('/products', require('./routes/productsRoute'))

app.set('PORT', process.env.PORT || 4000)

if (process.env.NODE_ENV !== 'test') {
  app.listen(app.set('PORT'), () => {
    console.log('Server running at', app.set('PORT'))
  })
}

module.exports = app
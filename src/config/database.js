const mysql = require('mysql')
const { promisify } = require('util')

const pool = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME
})

if (process.env.NODE_ENV === 'develop') {
  pool.getConnection((err, connection) => {
    if(err) {
      switch(err.code) {
        case 'PROTOCOL_CONNECTION_LOST':
          console.log('DATABASE CONNECTION WAS CLOSED')
          break
        case 'ER_CON_COUNT_ERROR':
          console.log('DATABASE HAS TOO MANY CONNECTIONS')
          break
        case 'ECONNREFUSED':
          console.log('DATABASE CONNECTION WAS REFUSED')
          break
        default:
          console.log('UKNOWN ERROR')
          break
      }
    }
    if(connection) {
      connection.release()
      console.log('DB is Connected')
      return;
    }
  });
}

const query = promisify(pool.query).bind(pool)

module.exports = { pool, query }

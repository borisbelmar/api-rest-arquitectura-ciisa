const { findAll, findById } = require('../controller/productsController')
const express = require('express')
const router = express.Router()

router.get('/', findAll)
router.get('/:id', findById)

module.exports = router
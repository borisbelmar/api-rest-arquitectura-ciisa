const supertest = require('supertest')
const app = require('../app')
const request = supertest(app)

jest.mock('../config/database', () => jest.fn())
const pool = require('../config/database')
pool.getConnection = jest.fn()

describe('Testing Express App Entrypoint', () => {
  it('should return message at root route', async done => {
    const res = await request.get('/')
    expect(res.status).toBe(200)
    expect(res.body.title).toEqual('Api de productos')
    done()
  })

  it('should return 404', async done => {
    const res = await request.get('/notexist')
    expect(res.status).toBe(404)
    done()
  })
})
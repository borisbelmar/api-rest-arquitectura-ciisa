const supertest = require('supertest')
const app = require('../app')
const request = supertest(app)

const products = require('./__mocks__/productsMock')

jest.mock('../config/database', () => ({ query: jest.fn() }))
let { query } = require('../config/database')

describe('Testing products routes', () => {
  it('Products route should return products', async done => {
    query.mockReturnValueOnce(products)

    const res = await request.get('/products')
    expect(res.body).toEqual(products)
    expect(res.status).toBe(200)
    done()
  })

  it('Products by id should return one products', async done => {
    const filteredProducts = products.filter(product => product.codigoProd === 1)
    query.mockReturnValueOnce(filteredProducts)

    const res = await request.get('/products/1')
    expect(res.body).toEqual(filteredProducts[0])
    expect(res.status).toBe(200)
    done()
  })

  it('Product by id should return 404', async done => {
    const res = await request.get('/products/5')
    expect(res.body).toEqual({})
    expect(res.status).toBe(404)
    done()
  })
})
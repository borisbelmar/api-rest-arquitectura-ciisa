const supertest = require('supertest')
const app = require('../app')
const request = supertest(app)
const { pool } = require('../config/database')

const products = require('../__test__/__mocks__/productsMock')

describe('Testing products routes', () => {
  afterAll(done => {
    pool.end()
    done()
  })
  
  it('Products route should return products', async done => {
    const res = await request.get('/products')
    expect(res.body).toEqual(products)
    expect(res.status).toBe(200)
    done()
  })

  it('Products by id should return product with id 3', async done => {
    const filteredProducts = products.filter(customer => customer.codigoProd === 3)
    const res = await request.get('/products/3')
    expect(res.body).toEqual(filteredProducts[0])
    expect(res.status).toBe(200)
    done()
  })

  it('Products by id should return 404', async done => {
    const res = await request.get('/products/5')
    expect(res.body).toEqual({})
    expect(res.status).toBe(404)
    done()
  })
})
module.exports = [
  {
    codigoProd: 1,
    nombreProd: 'Fan 120mm',
    precioProd: 13000
  },
  {
    codigoProd: 2,
    nombreProd: 'Ryzen 5',
    precioProd: 250000
  },
  {
    codigoProd: 3,
    nombreProd: 'RAM DDR4',
    precioProd: 89000
  },
  {
    codigoProd: 4,
    nombreProd: 'Teclado',
    precioProd: 25000
  }
]

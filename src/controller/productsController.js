const { query } = require('../config/database')

const findAll = async (_req, res) => {
  try {
    const data = await query(`
      SELECT codigoProd, nombreProd, precioProd FROM tblProductos
    `)
    res.json(data)
  } catch(err) {
    res.sendStatus(500)
  }
}

const findById = async (req, res) => {
  try {
    const id = req.params.id
    const data = await query(`
      SELECT codigoProd, nombreProd, precioProd FROM tblProductos WHERE codigoProd = ?
    `, [id])
    if(data && data.length > 0) {
      res.json(data[0])
    } else {
      res.sendStatus(404)
    }
  } catch(err) {
    console.log(err)
    res.sendStatus(500)
  }
}

module.exports = {
  findAll,
  findById
}
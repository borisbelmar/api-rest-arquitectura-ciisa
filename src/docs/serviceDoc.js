module.exports = {
  title: 'Api de productos',
  author: 'Boris Belmar <borisbelmarm@gmail.com>',
  routes: [
    { 
      endpoint: '/',
      method: 'GET',
      desc: 'Retorna un json con la información del servicio'
    },
    {
      endpoint: '/products',
      method: 'GET',
      desc: 'Retorna un json con un array de todos los productos desde la base de datos'
    },
    {
      endpoint: '/products/:id',
      method: 'GET',
      params: ['id:number'],
      desc: 'Retorna un json con el producto requerido'
    }
  ]
}
CREATE TABLE tblProductos (
  codigoProd INT AUTO_INCREMENT PRIMARY KEY,
  nombreProd VARCHAR(20) NOT NULL,
  precioProd INT NOT NULL
);

INSERT INTO tblProductos(nombreProd, precioProd) VALUES
	('Fan 120mm', 13000),
  ('Ryzen 5', 250000),
  ('RAM DDR4', 89000),
  ('Teclado', 25000);
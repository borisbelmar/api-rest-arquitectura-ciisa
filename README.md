
Servicio Rest para Arquitectura
---
##### Autor: Boris Belmar

### Sobre el proyecto
Este proyecto ha sido desarrollado en **NodeJs** utilizando el framework **Express** y la base de datos **MySQL**. El servicio tiene finalidades educativas y ha sido creado para el ramo de **Arquitectura de Soluciones del Instituto CIISA**.

### Requisitos
Es requerido **nodejs**, **npm** y un servidor de **mysql** corriendo. En la base de datos es necesario ejecutar el script *database/db.sql*. También es necesario crear un fichero **.env** para la configuración de variables de entornos, las cuales se especifican en el fichero *example.env*.

### Instalación de dependencias
```bash
npm install
```
### Ejecución del proyecto en producción
```bash
npm start
```

### Ejecución del proyecto en desarrollo
```bash
npm run dev
```

### Ejecución de pruebas
```bash
npm test
```

### Endpoints

**GET** / - Retorna información de la API.

**GET** /products - Retorna un arreglo con todos los productos.

**GET** /products/:id - Retorna un objeto con el id especificado.
